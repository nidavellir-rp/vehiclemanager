# Vehicle Manager

This resource makes creating vehicles simple, it checks if the surrounding area is safe to spawn in, using the length of the vehicle model to check the area. It also contains the option to "preview" vehicles (client-side rather than server side), and deleting vehicles in a roleplay fashion, making all players exit the vehicle before deleting it.

### This resource requires OneSync to be enabled on the server!
Tested on build 2967+

### Configuration

```lua
-- Disable machine guns built into cars, like the Nightshark
disable_vehicle_weapons 'true'

-- Carjacking features
-- Prevents locked vehicles from being jacked
disable_carjacking 'true'

-- When false living NPCs, driving locked cars, will drive away from the player
disable_carjacking_peds_drive_away 'false'
```

## Usage
### Creating a vehicle

```lua
-- We're spawning an Adder
local vehicleModel = "adder"
-- We're going to place the vehicle 5 meters in front of the player ped
local coords = GetOffsetFromEntityInWorldCoords(PlayerPedId(), 0, 5.0, 0)
-- Heading 0
local heading = 0

local response = exports["vehicle-manager"]:SpawnVehicle(vehicleModel, coords, heading)
print(json.encode(response))
-- Example printout: {"Success":true,"Message":"Vehicle of the type hakuchou 770 should have been created","VehicleId":770}
```

### Preview vehicle
These vehicles are client-side only, and don't appear to other players. These should only be used when players are browsing vehicles for themselves.
Hence why the script will lock the vehicle in place.
```lua
-- We're spawning an Adder
local vehicleModel = "adder"
-- We're going to place the vehicle 5 meters in front of the player ped
local coords = GetOffsetFromEntityInWorldCoords(PlayerPedId(), 0, 5.0, 0)
-- Heading 0
local heading = 0

local response = exports["vehicle-manager"]:PreviewVehicle(vehicleModel, coords, heading)
print(json.encode(response))
-- Example printout: {"Success":true,"Message":"Vehicle of the type hakuchou 770 should have been created","VehicleId":770}
```

### Delete vehicle
```lua
local ped = PlayerPedId()

if IsPedInAnyVehicle(ped, false) then
    local vehicle = GetVehiclePedIsIn(ped, false)

    -- Send a command to tell all player peds to exit the vehicle (RP-style) before it is deleted
    -- All non-player peds will get deleted before the vehicle is deleted
    local shouldAllPlayerPedsExitBeforeDeleting = true
    
    -- Returns a boolean value
    local success = exports["vehicle-manager"]:DeleteVehicle(vehicle, shouldAllPlayerPedsExitBeforeDeleting)
    print(json.encode(success))
    -- Example printout: true 
end
```

## Functions

### SpawnVehicle(string modelName, Vector3 location, float heading)

Spawn a vehicle without checking the surroundings for other vehicles or objects

### SpawnVehicleByHash(int hashKey, Vector3, location, float heading, bool doSafetyCheck, float customSafeDistance)

Spawn a vehicle, optionally doing the safe surroundings check for vehicles. If a `customSafeDistance` is not set, or is a negative number, we use the length of the vehicle as safety radius.

### SafelySpawnVehicle(string modelName, Vector3 location, float heading)

Spawn a vehicle while checking the surroundings for other vehicles that could block it

### PreviewVehicle(string modelName, Vector3 location, float heading)

Spawn a client-side vehicle without checking the surroundings for other vehicles

### SafelySpawnVehicle(string modelName, Vector3 location, float heading)

Spawn a client-side vehicle while checking the surroundings for other vehicles

### DeleteVehicle(int vehicle, bool shouldAllPlayerPedsExitBeforeDeleting)

Delete the vehicle, when the **boolean parameter** is set as **true**, the player peds in the car will exit the vehicle before it can be deleted

### LockVehicle(int vehicle)

Lock the vehicle doors. Players will not be able to open doors, or break the window, to enter the vehicle

### UnlockVehicle(int vehicle)

Unlock the vehicle doors, players will be able to open the vehicle doors.

### IsVehicleLocked(int vehicle)

Checks the status of the vehicle, if it's genuinely locked

## Events
### vehicleManager:model:loading
The resource has started loading a model into the client's memory

### vehicleManager:model:loaded
The resource has finished loading a model into the client's memory

### vehicleManager:cancelled
The vehicle will not be created

### vehicleManager:done
The vehicle should have been created

### vehicleManager:server:create
Server will start creating the vehicle

### vehicleManager:created
Server reports the vehicle has been created
        
