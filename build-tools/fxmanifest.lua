fx_version 'adamant'
games {'gta5'}

author 'Nidavellir RP'
description 'Vehicle spawning resource, that checks the surrounding area using the length of the model before spawning it.'
version 'custom-build'

-- Disable machine guns on cars, like the Nightshark
-- disable_vehicle_weapons 'true'

-- Carjacking features
disable_carjacking 'true'
disable_carjacking_peds_drive_away 'false'

-- What type of locking is done for cars
-- Valid values: CanBeBrokenInto, CanBeBrokenIntoPersist, CannotBeTriedToEnter, Locked, LockedForPlayer, StickPlayerInside
-- vehicle_locking_type 'CanBeBrokenInto' -- Default value: Locked

client_script 'client.net.dll'
server_script 'server.net.dll'