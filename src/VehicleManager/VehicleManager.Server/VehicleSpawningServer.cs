﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using static CitizenFX.Core.Native.API;

namespace VehicleManager.Server
{
    public class VehicleSpawningServer : BaseScript
    {
        /// <summary>
        /// Check for vehicles in range of any location
        /// </summary>
        /// <param name="location"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private bool AnyVehicleNearPoint(Vector3 location, float distance)
        {
            // Distance should be greater than zero if you want to know if any vehicles are in area
            if (float.IsNaN(distance) || distance <= 0)
            {
                return false;
            }

            List<object> vehicles = GetAllVehicles();

            return vehicles?.Any(x => VehicleDistance(x, location) <= distance) ?? false;
        }

        /// <summary>
        /// Ensure the entity exists before doing calculations
        /// </summary>
        /// <param name="value"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        private float VehicleDistance(object value, Vector3 location)
        {
            if (!(value is null))
            {
                if (int.TryParse(value.ToString(), out int handle))
                {
                    var vehicle = Entity.FromHandle(handle);
                    return vehicle?.Position.DistanceToSquared(location) ?? float.PositiveInfinity;
                }
            }

            return float.PositiveInfinity;
        }

        /// <summary>
        /// Create a vehicle server side
        /// </summary>
        /// <param name="player"></param>
        /// <param name="modelHash"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="heading"></param>
        /// <param name="requestId"></param>
        [EventHandler("vehicleManager:server:create")]
        private async void Create([FromSource] Player player, uint modelHash, Vector3 spawnLocation, float heading, float distance, string requestId)
        {
            if (AnyVehicleNearPoint(spawnLocation, distance))
            {
                Debug.WriteLine($"{player?.Name} ({player?.Identifiers.FirstOrDefault()}) requested a vehicle near {spawnLocation}. Another vehicle blocked the spawn. Safe distance: {distance}.");
                player?.TriggerEvent("vehicleManager:cancelled", requestId);
                return;
            }

            // Spawn a vehicle
            int vehicle = CreateVehicle(modelHash, spawnLocation.X, spawnLocation.Y, spawnLocation.Z, heading, true, true);

            if (vehicle == 0)
            {
                // Vehicle not created, likely due to OneSync not being enabled or being outdated
                Debug.WriteLine($"Could not create a vehicle with model hash {modelHash}, requested by {player?.Handle}. This is likely due to OneSync being disabled, or artifacts outdated");
                return;
            }

            // Set a maximum wait time for entity being created
            var maxWaitTime = DateTime.UtcNow.AddSeconds(5);

            while (!DoesEntityExist(vehicle))
            {
                // No delay as we wait for entity to be created
                await Delay(0);

                // If maximum wait time has expired, we break out of the loop
                if (maxWaitTime < DateTime.UtcNow) break;
            }

            if (!DoesEntityExist(vehicle))
            {
                // Vehicle still hasn't been created...
                Debug.WriteLine($"Failed to create a vehicle {modelHash}, requested by {player?.Handle}. Entity was not created in a timely manner");
                return;
            }

            player?.TriggerEvent("vehicleManager:created", requestId, NetworkGetNetworkIdFromEntity(vehicle));
        }

        /// <summary>
        /// Tell all peds in a given vehicle to exit it
        /// </summary>
        /// <param name="vehicleNetworkId"></param>
        [EventHandler("vehicleManager:server:tellPedsToExitVehicle")]
        private void TaskPedToExitVehicle(int vehicleNetworkId)
        {
            TriggerClientEvent("vehicleManager:tellPedsToExitVehicle", vehicleNetworkId);
        }
    }
}
