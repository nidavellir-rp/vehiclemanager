﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleManager.Client
{
    public class ResponseModel
    {
        public bool Success { get; set; } = false;
        public int? VehicleId { get; set; }
        public string Message { get; set; }

        public ResponseModel(bool success, int? vehicleId, string message)
        {
            Success = success;
            VehicleId = vehicleId;
            Message = message;
        }
    }
}
