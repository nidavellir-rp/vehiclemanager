﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace VehicleManager.Client
{
    public class VehicleSpawning : BaseScript
    {
        private Dictionary<string, Vehicle> _vehicles = new Dictionary<string, Vehicle>();
        private List<string> _cancelledRequests = new List<string>();
        private int _requestId = 0;

        public VehicleSpawning()
        {
            EventHandlers.Add("onClientResourceStart", new Action<string>(OnStartup));
            EventHandlers.Add("vehicleManager:created", new Action<string, int>(VehicleCreatedCallback));
            EventHandlers.Add("vehicleManager:cancelled", new Action<string>(VehicleBlockedCallback));
            EventHandlers.Add("vehicleManager:tellPedsToExitVehicle", new Action<int>(ExitVehicle));
        }

        private void OnStartup(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            bool.TryParse(GetResourceMetadata(resourceName, "disable_vehicle_weapons", 0), out bool vehicleWeaponsDisabled);

            if (vehicleWeaponsDisabled)
            {
                // If set disabled, start ticking for disable vehicle weapons
                Tick += DisableVehicleWeapons;
            }

            // Safely create a vehicle in the world, the vehicle will be created server-side
            Exports.Add("SpawnVehicle", new Func<string, Vector3, float, Task<object>>(async (modelName, location, heading) => await SpawnVehicleAsync(GetHashKey(modelName), location, heading, false, false)));
            Exports.Add("SpawnVehicleByHash", new Func<int, Vector3, float, bool, float, Task<object>>(async (hashKey, location, heading, safetyCheck, customSafeDistance) => await SpawnVehicleAsync(hashKey, location, heading, false, safetyCheck, customSafeDistance)));
            Exports.Add("SafelySpawnVehicle", new Func<string, Vector3, float, Task<object>>(async (modelName, location, heading) => await SpawnVehicleAsync(GetHashKey(modelName), location, heading, false, true)));
            Exports.Add("DeleteVehicle", new Func<int, bool, Task<bool>>(DeleteAsync));
        }

        /// <summary>
        /// Safely create a vehicle in the world, either client or server side
        /// </summary>
        /// <param name="hashKey"></param>
        /// <param name="location"></param>
        /// <param name="heading"></param>
        /// <param name="isClientOnly"></param>
        /// <param name="hasSafetyCheck"></param>
        /// <param name="customSafeDistance"></param>
        /// <returns></returns>
        public async Task<object> SpawnVehicleAsync(int hashKey, Vector3 location, float heading, bool isClientOnly = false, bool hasSafetyCheck = false, float customSafeDistance = -1f)
        {
            var model = new Model(hashKey);

            if (!(model?.IsValid ?? false))
            {
                return new { Success = false, Message = "A vehicle of unknown type was requested" };
            }

            // Ensure that the model is loaded
            if (!(model?.IsLoaded ?? false))
            {
                // Request model
                model?.Request();

                // Wait for the model to load
                while (!(model?.IsLoaded ?? false))
                {
                    await Delay(0);
                }
            }

            float distance = float.NaN;

            // Do the safety check
            if (hasSafetyCheck)
            {
                distance = customSafeDistance > 0 ? customSafeDistance : model?.GetDimensions().Y ?? 0f;

                // Check if there's a vehicle blocking the spawn location
                if (IsAnyVehicleNearPoint(location.X, location.Y, location.Z, distance))
                {
                    return new { Success = false, Message = "Location is blocked by another vehicle" };
                }
            }

            Vehicle vehicle = null;

            // Generate an identifier for the vehicle spawning
            string requestId = $"{PlayerId()}:{hashKey}:{_requestId++}";

            // Create the vehicle server-side
            TriggerServerEvent("vehicleManager:server:create", (uint)model?.Hash, location, heading, distance, requestId);

            // Wait up to 10 seconds for confirmation that server spawned the vehicle
            var timeout = DateTime.UtcNow.AddSeconds(10);

            // Run through the timeout
            while (DateTime.UtcNow < timeout)
            {
                if (_vehicles?.ContainsKey(requestId) ?? false)
                {
                    // Confirmation from server received, break from the loop
                    break;
                }

                if (_cancelledRequests?.Contains(requestId) ?? false)
                {
                    // The request was cancelled because it's blocked by another vehicle
                    _cancelledRequests?.Remove(requestId);
                    return new { Success = false, Message = "Location is blocked by another vehicle" };
                }

                await Delay(0);
            }

            // Retrieve the vehicle with the appropriate request
            vehicle = _vehicles?.FirstOrDefault(x => x.Key.Equals(requestId, StringComparison.OrdinalIgnoreCase)).Value;
            // Remove the request ID as we've completed the task
            _vehicles?.Remove(requestId);

            // Free the model from memory
            model?.MarkAsNoLongerNeeded();

            // Wait up to 5 seconds to ensure the vehicle exists
            timeout = DateTime.UtcNow.AddSeconds(5);

            // Run through the timeout
            while (DateTime.UtcNow < timeout)
            {
                if (vehicle?.Exists() ?? false)
                {
                    break;
                }

                await Delay(0);
            }

            if (vehicle?.Exists() ?? false)
            {
                // Set the basic values to maintain the vehicle in the world
                vehicle.IsPersistent = true;
                vehicle.PreviouslyOwnedByPlayer = true;
                SetNetworkIdExistsOnAllMachines(vehicle.NetworkId, true);
                SetNetworkIdCanMigrate(vehicle.NetworkId, true);
                vehicle?.PlaceOnGround();
                vehicle.RadioStation = RadioStation.RadioOff;

                if (!NetworkHasControlOfEntity(vehicle.Handle))
                {
                    NetworkRequestControlOfEntity(vehicle.Handle);

                    timeout = DateTime.UtcNow.AddSeconds(5);
                    while (!NetworkHasControlOfEntity(vehicle.Handle))
                    {
                        await Delay(0);

                        if (timeout < DateTime.UtcNow)
                        {
                            Debug.WriteLine($"Could not claim ownership of entity {vehicle?.Handle} ({vehicle?.NetworkId}) in a timely manner");
                            break;
                        }
                    }
                }

                return new { Success = true, Message = $"Vehicle {vehicle?.DisplayName} successfully spawned.", VehicleId = vehicle?.Handle, NetworkId = vehicle?.NetworkId };
            }

            return new { Success = false, Message = $"Could not spawn vehicle of type {hashKey}" };
        }

        /// <summary>
        /// When enabled disables the machinegun weapon in vehicles
        /// </summary>
        /// <returns></returns>
        public async Task DisableVehicleWeapons()
        {
            Ped playerPed = Game.PlayerPed;

            if (playerPed?.IsInVehicle() ?? false)
            {
                if (!playerPed?.CurrentVehicle?.Model?.IsPlane ?? false)
                {
                    DisableVehicleWeapon(true, 2786772340, playerPed.CurrentVehicle.Handle, playerPed.Handle);
                    SetCanPedSelectWeapon(playerPed.Handle, 2786772340, false);
                }
            }

            await Delay(50);
        }

        /// <summary>
        /// Vehicle callback used to inform the client that the server won't be spawning the vehicle
        /// </summary>
        /// <param name="requestId"></param>
        private async void VehicleBlockedCallback(string requestId)
        {
            _cancelledRequests?.Add(requestId);
        }

        /// <summary>
        /// Vehicle callback used to inform the client that server has completed its task
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="vehicleNetworkId"></param>
        private async void VehicleCreatedCallback(string requestId, int vehicleNetworkId)
        {
            DateTime timeout = DateTime.UtcNow.AddSeconds(5);
            while (!NetworkDoesNetworkIdExist(vehicleNetworkId))
            {
                await Delay(0);

                if (timeout < DateTime.UtcNow)
                {
                    Debug.WriteLine($"Timed out waiting on entity with network id {vehicleNetworkId} to exist");
                    return;
                }
            }

            Vehicle vehicle = (Vehicle)Entity.FromNetworkId(vehicleNetworkId);

            // Ensure that we have control of the entity
            if (!NetworkHasControlOfEntity(vehicle.Handle))
            {
                NetworkRequestControlOfEntity(vehicle.Handle);
            }

            // Vehicle created, add the request & vehicle ID to the dictionary
            _vehicles?.Add(requestId, vehicle);
        }

        /// <summary>
        /// Event to inform anyone within a specific vehicle to exit it
        /// </summary>
        /// <param name="vehicleNetworkId"></param>
        private void ExitVehicle(int vehicleNetworkId)
        {
            // Get the player's ped
            Ped ped = Game.PlayerPed;

            // Get the vehicle
            var vehicle = (Vehicle)Entity.FromNetworkId(vehicleNetworkId);

            // If this ped is in the vehicle, have them exit it
            if (ped?.IsInVehicle(vehicle) ?? false)
            {
                ped?.Task?.ClearAll();
                ped?.Task?.LeaveVehicle(LeaveVehicleFlags.None);
            }
        }

        /// <summary>
        /// Remove a vehicle from the world
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="shouldPlayersExitVehicle">When true the player peds will be tasked with exiting the vehicle before it is deleted</param>
        private async Task<bool> DeleteAsync(int vehicleId, bool shouldPlayersExitVehicle)
        {
            var vehicle = new Vehicle(vehicleId);

            if (vehicle == null || (!vehicle?.Exists() ?? false))
            {
                // No entity exists for this vehicle ID
                return false;
            }

            if (!vehicle?.Model?.IsVehicle ?? false)
            {
                // Entity isn't a vehicle, don't delete it!
                return false;
            }

            // Ensure that passengers exit the vehicle
            if (shouldPlayersExitVehicle && (vehicle?.Occupants?.Any(x => x.IsPlayer) ?? false))
            {
                // Make the player occupants exit the vehicle
                TriggerServerEvent("vehicleManager:server:tellPedsToExitVehicle", vehicle?.NetworkId);

                var maxWaitTime = DateTime.UtcNow.AddSeconds(4);

                while (vehicle?.Occupants?.Any(x => x?.IsPlayer ?? false && (x?.IsInVehicle() ?? false)) ?? false)
                {
                    // Wait for the players to exit the vehicle
                    await Delay(0);

                    if (maxWaitTime < DateTime.UtcNow) break;
                }

                // Wait for the peds to exit the vehicle
                await Delay(1000);
            }

            if (!vehicle?.IsPersistent ?? false)
            {
                // Ensure that the vehicle is a mission entity so that we can delete it
                vehicle.IsPersistent = true;
            }

            // Check if we need to get ownership fo the entity
            if (NetworkDoesEntityExistWithNetworkId(vehicle.NetworkId) && !NetworkHasControlOfNetworkId(vehicle.NetworkId))
            {
                // Ensure we have control of the entity
                NetworkRequestControlOfNetworkId(vehicle.NetworkId);

                // Wait for the ownership to happen
                while (!NetworkHasControlOfNetworkId(vehicle.NetworkId))
                {
                    await Delay(0);

                    if (!NetworkDoesEntityExistWithNetworkId(vehicle.NetworkId))
                    {
                        Debug.WriteLine($"Could not claim ownership of network ID {vehicle?.NetworkId}");
                        break;
                    }
                }
            }

            // Remove the vehicle
            vehicle?.Delete();
            vehicle?.MarkAsNoLongerNeeded();
            return true;
        }
    }
}
