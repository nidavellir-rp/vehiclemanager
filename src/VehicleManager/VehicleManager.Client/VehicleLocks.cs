﻿using CitizenFX.Core;
using System;
using System.Linq;
using System.Threading.Tasks;
using static CitizenFX.Core.Native.API;

namespace VehicleManager.Client
{
    public class VehicleLocks : BaseScript
    {
        private VehicleLockStatus[] _lockStatuses = new VehicleLockStatus[] { VehicleLockStatus.Locked, VehicleLockStatus.LockedForPlayer, VehicleLockStatus.CannotBeTriedToEnter, VehicleLockStatus.CanBeBrokenInto, VehicleLockStatus.CanBeBrokenIntoPersist };
        private VehicleLockStatus _vehicleLockStatus = VehicleLockStatus.Locked;
        private bool _shouldPedFleeOnFailedCarjacking = true;

        [EventHandler("onClientResourceStart")]
        private void OnStartup(string resourceName)
        {
            if (GetCurrentResourceName() != resourceName) return;

            // Override the vehicle lock status
            OverrideVehicleLockStatus();

            Exports.Add("LockVehicle", new Func<int, bool>((entity) => ChangeVehicleLockedState((Vehicle)Entity.FromHandle(entity), _vehicleLockStatus)));
            Exports.Add("SetVehicleLockState", new Func<int, string, bool>((entity, lockType) => ChangeVehicleLockedState((Vehicle)Entity.FromHandle(entity), (VehicleLockStatus)Enum.Parse(typeof(VehicleLockStatus), lockType, true))));
            Exports.Add("UnlockVehicle", new Func<int, bool>((entity) => ChangeVehicleLockedState((Vehicle)Entity.FromHandle(entity), VehicleLockStatus.Unlocked)));
            Exports.Add("IsVehicleLocked", new Func<int, bool>((entity) => IsVehicleLocked((Vehicle)Entity.FromHandle(entity))));

            // Get the configuration value for carjacking
            bool.TryParse(GetResourceMetadata(resourceName, "disable_carjacking", 0), out bool preventEnteringLockedVehicle);

            if (preventEnteringLockedVehicle)
            {
                // Car jacking of locked vehicles has been disabled

                // Check if the AI should drive away if player tries to enter their locked vehicle
                bool.TryParse(GetResourceMetadata(resourceName, "disable_carjacking_peds_drive_away", 0), out bool pedDoesNotRespondToCarjacking);
                _shouldPedFleeOnFailedCarjacking = !pedDoesNotRespondToCarjacking;

                // Start running the per-tick checks
                Tick += PreventEnteringLockedVehicle;
            }
        }

        /// <summary>
        /// Check if the override variable has been set, and convert it into a valid lock status
        /// </summary>
        private void OverrideVehicleLockStatus()
        {
            // Override the vehicle lock status
            if (Enum.TryParse(GetResourceMetadata(GetCurrentResourceName(), "vehicle_locking_type", 0), true, out VehicleLockStatus vehicleLockStatus))
            {
                _vehicleLockStatus = vehicleLockStatus;
            }
        }

        /// <summary>
        /// Update the lock state of a vehicle
        /// </summary>
        /// <param name="vehicle"></param>
        /// <param name="setIsLocked"></param>
        /// <returns></returns>
        public bool ChangeVehicleLockedState(Vehicle vehicle, VehicleLockStatus status)
        {
            // Check if the vehicle exists
            if (vehicle?.Exists() ?? false)
            {
                // Check if the vehicle has been destroyed
                if (vehicle?.IsAlive ?? false)
                {
                    Debug.WriteLine($"Status: {status}");
                    // Set the new lock state
                    vehicle.LockStatus = status;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Check if a vehicle is in one of the locked states
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        public bool IsVehicleLocked(Vehicle vehicle) => _lockStatuses?.Any(x => x == vehicle?.LockStatus) ?? false;

        /// <summary>
        /// Runs on ticks to check if the player is trying to enter a vehicle. They will be prevented from entering locked vehicles
        /// </summary>
        /// <returns></returns>
        public async Task PreventEnteringLockedVehicle()
        {
            Ped ped = Game.PlayerPed;

            if (ped?.IsTryingToEnterALockedVehicle ?? false)
            {
                // Get the vehicle the player tried to enter
                Vehicle vehicle = ped?.VehicleTryingToEnter;

                // Clear the tasks
                ped?.Task?.ClearAll();
                TriggerEvent("vehicleManager:locks:tried-to-enter-vehicle", ped?.NetworkId, ped?.Position, vehicle?.NetworkId, vehicle?.ClassLocalizedName);

                // Get the driver of the vehicle
                Ped driver = vehicle?.Driver;

                // Check if we're making locals flee on failed carjackings
                if (_shouldPedFleeOnFailedCarjacking)
                {
                    // The driver is not a player, are they fleeing?
                    if ((!driver?.IsPlayer ?? false) && (!driver?.IsFleeing ?? false) && (driver?.IsAlive ?? false))
                    {
                        // Change the lock status to prevent the player from gridlocking the ped
                        vehicle.LockStatus = VehicleLockStatus.CannotBeTriedToEnter;

                        // Check if we need to get ownership fo the driver
                        if ((driver?.Exists() ?? false) && !NetworkHasControlOfNetworkId(driver.NetworkId))
                        {
                            // Check if we need to get ownership fo the entity
                            if (!NetworkHasControlOfNetworkId(driver.NetworkId))
                            {
                                // Ensure we have control of the entity
                                NetworkRequestControlOfNetworkId(driver.NetworkId);

                                // Wait for the ownership to happen
                                while (!NetworkHasControlOfNetworkId(driver.NetworkId))
                                {
                                    await Delay(0);
                                }
                            }
                        }

                        // Tell the driver to flee from the player
                        driver?.Task?.FleeFrom(ped);
                    }
                }
            }

            await Delay(500);
        }
    }
}
